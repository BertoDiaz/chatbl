"""
Copyright (C) 2018  Heriberto J. Díaz Luis-Ravelo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from view import View
from lib import SerialPort
from PyQt5.QtBluetooth import QBluetoothDeviceDiscoveryAgent, QBluetoothDeviceInfo, QBluetoothSocket
from PyQt5.QtBluetooth import QBluetoothServiceInfo, QBluetoothLocalDevice
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QObject, QIODevice
from functools import partial
import sys


class Controller(QObject):
    def __init__(self):
        super().__init__()

        self.discoveredDevices = []
        self.discoveredNormalDevices = []
        self.discoveredLowEnergyDevices = []
        self.deviceSelected = None
        self.socketBL = None
        self.serverBL = None
        self.clientBL = None
        self.localBL = QBluetoothLocalDevice(self)
        self.serviceBL = QBluetoothServiceInfo()
        self.discoveryAgent = None
        self.connectedBL = False
        self.modeBL = None
        self.numberOfItem = None

        self.serialPort = SerialPort.SerialPort()

        self.view = View(None)

        self.view.mainWindow()

        self.view.show()

        self.view.closeEventSignal.connect(self.closeEvent)

        self.view.addComboBox('PORTS')

        self.view.btnExit.clicked.connect(self.exit_App)
        self.view.btnScan.clicked.connect(self.startScan)
        self.view.btnSend.clicked.connect(self.sendMessageClient)
        self.view.cmbPorts.activated.connect(self.onActivated)

        self.view.lstDevices.currentRowChanged.connect(self.selectedDevice)

        self.view.btnConnect.clicked.connect(self.connectBL)
        self.view.btnDisconnect.clicked.connect(self.disconnectBL)

        self.discoveryAgent = QBluetoothDeviceDiscoveryAgent(self)

        self.socketBL = QBluetoothSocket(QBluetoothServiceInfo.RfcommProtocol)

        self.startScan()

    def startScan(self):
        self.view.setBtnScanState(True)
        self.view.setBtnScanDisable(True)

        self.deviceSelected = None
        self.view.clearDevicesFromList()

        self.discoveredDevices = []
        self.discoveredNormalDevices = []
        self.discoveredLowEnergyDevices = []

        self.discoveryAgent.deviceDiscovered.connect(self.addNormalDevice)
        self.discoveryAgent.finished.connect(self.finishedNormalScan)

        self.view.setLstScannerStateText('Scanning...')

        self.discoveryAgent.start(QBluetoothDeviceDiscoveryAgent.DiscoveryMethod(1))

    def addNormalDevice(self, device):
        self.discoveredDevices.append(QBluetoothDeviceInfo(device))

        self.discoveredNormalDevices.append(QBluetoothDeviceInfo(device))

        self.view.addDevicesToList(QBluetoothDeviceInfo(device).deviceUuid().toString(),
                                   QBluetoothDeviceInfo(device).name())

    def addLowEnergyDevice(self, device):
        self.discoveredDevices.append(QBluetoothDeviceInfo(device))

        self.discoveredLowEnergyDevices.append(QBluetoothDeviceInfo(device))

        self.view.addDevicesToList(QBluetoothDeviceInfo(device).deviceUuid().toString(),
                                   QBluetoothDeviceInfo(device).name())

    def finishedNormalScan(self):
        self.view.setLstScannerStateText('Stopped')

        self.discoveryAgent.stop()

        self.discoveryAgent.deviceDiscovered.disconnect()
        self.discoveryAgent.finished.disconnect()
        self.discoveryAgent.deviceDiscovered.connect(self.addLowEnergyDevice)
        self.discoveryAgent.finished.connect(self.finishedLowEnergyScan)

        self.view.setLstScannerStateText('Scanning...')

        self.discoveryAgent.start(QBluetoothDeviceDiscoveryAgent.DiscoveryMethod(2))

    def finishedLowEnergyScan(self):
        self.view.setBtnScanState(False)
        self.view.setBtnScanDisable(False)

        self.view.setLstScannerStateText('Stopped')

        self.discoveryAgent.stop()

        self.discoveryAgent.deviceDiscovered.disconnect()
        self.discoveryAgent.finished.disconnect()

    def selectedDevice(self, item):
        self.deviceSelected = self.discoveredDevices[item]

    def selectedNormalDevice(self, item):
        self.deviceSelected = self.discoveredNormalDevices[item]

    def selectedLowEnergyDevice(self, item):
        self.deviceSelected = self.discoveredLowEnergyDevices[item]

    def connectBL(self):
        if self.deviceSelected is not None:
            self.view.setBtnConnectDisable(True)

            try:
                self.discoveryAgent.stop()
                self.discoveryAgent.deviceDiscovered.disconnect()

            except TypeError:
                pass

            self.socketBL.connectToService(self.deviceSelected.address(), self.deviceSelected.deviceUuid(),
                                           QIODevice.ReadWrite)

            print(self.socketBL.localPort())
            self.view.setLblOpenPortText(str(self.socketBL.localPort()))

            socketBLConnected = partial(self.socketBLConnected, port=self.socketBL.localPort())

            self.socketBL.connected.connect(socketBLConnected)
            self.socketBL.disconnected.connect(self.socketBLDisconnected)
            self.socketBL.error.connect(self.socketBLError)
            self.socketBL.readyRead.connect(self.socketBLRead)

        else:
            self.view.setMessageCritical('You have to select a device.')

    def disconnectBL(self):
        self.view.setBtnDisconnectDisable(True)

        self.socketBL.disconnectFromService()

    @staticmethod
    def socketBLError(error):
        print(error)

    def socketBLConnected(self, port):
        self.view.setBtnDisconnectDisable(False)
        self.view.setLstScannerStateText('Connected')
        self.view.setBtnSendDisable(False)

        ports = self.serialPort.ask_for_port()

        self.view.clearComboBox()
        self.view.setCmbPortsDisable(False)
        self.view.addComboBox('PORTS')

        for port in ports:
            self.view.addComboBox(port.portName())

    def socketBLDisconnected(self):
        self.view.setBtnScanDisable(False)
        self.view.setBtnConnectDisable(False)
        self.view.setLstScannerStateText('Disconnected')
        self.view.setBtnSendDisable(True)

    def socketBLRead(self):
        if self.socketBL.canReadLine():
            self.view.setEdtMessagesReceiveText(self.socketBL.readAll())

    def sendMessageClient(self):
        message = self.view.getEdtSendMessageText()
        self.serialPort.write_port_list(message)
        self.view.clearEdtSendMessage()
        self.view.setEdtMessagesSendText(message)

    def onActivated(self, numberItem):
        """Change the value of the comboBox.

        When a different value of the comboBox is chosen, the numberOfItem variable is changed.

        Args:
            numberItem (int): the position number of the new value of the comboBox.
        """
        if numberItem > 0:
            self.numberOfItem = numberItem
            if self.serialPort.open_port(self.numberOfItem):
                self.view.setCmbPortsDisable(True)

                self.view.setLblOpenPortText('Port Connected')
                self.view.setGreenTextLabel()

                """-------------------------------------- Serial Port Connect ---------------------------------------"""

                self.serialPort.serialPort.readyRead.connect(self.serialPort.receive_multiple_data)
                self.serialPort.packet_received.connect(self.dataReceived)

                """------------------------------------ End Serial Port Connect -------------------------------------"""

    def dataReceived(self, data):
        self.view.setEdtMessagesReceiveText(data)

    def closeEvent(self, event):
        """Change the view to adapt to the new window size."""
        exitApp = self.view.setMessageExit()

        if exitApp:
            event.accept()

        else:
            event.ignore()

    def exit_App(self):
        """Exit of the app.

        This function show the message to exit of the app and close the app.
        """
        self.view.close()


if __name__ == '__main__':
    app = QApplication([])

    window = Controller()
    sys.exit(app.exec_())
