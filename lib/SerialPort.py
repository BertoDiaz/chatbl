"""
Copyright (C) 2018  Heriberto J. Díaz Luis-Ravelo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt5.QtCore import QObject, QIODevice, pyqtSignal
from PyQt5.QtSerialPort import QSerialPort, QSerialPortInfo
from serial.tools.list_ports import comports


class SerialPort(QObject):
    packet_received = pyqtSignal(str)

    def __init__(self):
        super(SerialPort, self).__init__()

        """-------------------------------------------- Global Variables --------------------------------------------"""

        self.ports = []
        self.serialData = ''
        self.commands = {
            'CurveTemporal': '|',
            'GainOffset': ':',
            'Laser': '#',
            'TimeAverage': '&',
            'StopTemporal': '[',
            'StopTechnical': '.',
            'ControlPeris': '*',
            'ControlImpulA': '!',
            'ControlImpulB': '+',
            'BackPeris': '<',
            'StopPeris': '=',
            'ForwardPeris': '>',
            'TimePulsesPumps': '_',
            'VolumePurge': 'a7',
            'PurgeImpulA': '{',
            'PurgeImpulB': '}',
            'IAmAlive': '?',
            'PowerDown': '/',
            'FinishExperiment': '[',
            'Peristaltic': '*',
            'ImpulsionalA': '!',
            'ImpulsionalB': '+',
            'InitExperiment': '|',
            'ACK': '@'
        }

        self.serialPort = QSerialPort()

        """------------------------------------------ End Global Variables ------------------------------------------"""

    def ask_for_port(self):
        """Return a list of ports.

        Returns:
            list: the list with the ports found in the PC.
        """
        self.ports = QSerialPortInfo.availablePorts()

        return self.ports

    def open_port(self, numberOfItem):
        """Open the connection with the port selected and some parameters of the port are configured.

        Args:
            numberOfItem (int): the number of port to connect.

        Returns:
            bool: if the connection was allowed.
        """
        if numberOfItem != 0:
            item = self.ports[numberOfItem - 1]

            # print(item['port'])

            self.serialPort.setPortName(item.portName())
            portOpen = self.serialPort.open(QIODevice.ReadWrite)

            if portOpen:
                self.serialPort.setBaudRate(QSerialPort.Baud115200)
                self.serialPort.setDataBits(QSerialPort.Data8)
                self.serialPort.setParity(QSerialPort.NoParity)
                self.serialPort.setStopBits(QSerialPort.OneStop)
                self.serialPort.setFlowControl(QSerialPort.NoFlowControl)

                return True

            else:
                return False

    def close_port(self):
        """Close the connection with the port."""
        self.serialPort.close()

    def receive_port(self):
        """Return a list with the characters received by the COM port, but first this is decoded to UTF-8.

        Returns:
            list: it is a list with the character received.
        """
        data = self.serialPort.readAll()
        self.serialData = data.data().decode('utf8')

        return self.serialData

    def receive_data(self):
        """Active the pyqtSygnal to receive a data of the COM port, but first this is decoded to UTF-8."""
        dataRead = self.serialPort.read(1).decode('utf8')

        self.packet_received.emit(dataRead)

    def receive_multiple_data(self):
        """Active the pyqtSygnal to receive all data of the COM port, but first this is decoded to UTF-8."""
        data = self.serialPort.readAll()

        dataRead = data.data().decode('utf8')

        self.packet_received.emit(dataRead)

    def write_port(self, data):
        """Write the data to be sent by the COM port.

        Args:
            data (character): the character to send.
        """
        return self.serialPort.writeData(data.encode())

    def write_port_list(self, data):
        """Write the data to be sent by the COM port.

        Args:
            data (list): the list with the character to send.
        """
        toReturn = None

        for value in data:
            toReturn = self.serialPort.writeData(value.encode())

        return toReturn

    def send_Command_ACK(self, how):
        """Send the ACK command (@) by the COM port."""
        toSend = self.commands['ACK'] + self.commands[how]

        return self.write_port_list(toSend)

    def send_I_am_alive(self):
        """Send the I am alive command (?) by the COM port."""
        self.write_port('?')

    def send_Gain_Offset(self, toSend):
        """Send the GainOffset command (:) with the data in hex by the COM port.

        Args:
            toSend (list): the list with the value to send.
        """
        self.write_port(self.commands['GainOffset'])

        for value in toSend:
            self.write_port(f'{value:02x}')

    def send_Control_Peristaltic(self, toSend):
        """Send the ControlPeris command (*) with the data in hex by the COM port.

        Args:
            toSend (list): the list with the value to send.
        """
        self.write_port(self.commands['ControlPeris'])

        for value in toSend:
            self.write_port(f'{value:02x}')

    def send_Control_Impul_A(self, toSend):
        """Send the ControlImpulA command (!) with the data in hex by the COM port.

        Args:
            toSend (list): the list with the value to send.
        """
        self.write_port(self.commands['ControlImpulA'])

        # for value in toSend:
        self.write_port(f'{toSend:04x}')

    def send_Control_Impul_B(self, toSend):
        """Send the ControlImpulB command (+) with the data in hex by the COM port.

        Args:
            toSend (list): the list with the value to send.
        """
        self.write_port(self.commands['ControlImpulB'])

        # for value in toSend:
        self.write_port(f'{toSend:04x}')

    def send_Volume_Purges(self, toSend):
        """Send the VolumePurge command (a7) with the data in hex by the COM port.

        Args:
            toSend (list): the list with the value to send.
        """
        self.write_port(self.commands['VolumePurge'])

        for value in toSend:
            self.write_port(f'{value:04x}')

    def send_Laser(self, toSend):
        """Send the Laser command (#) with the data in hex by the COM port.

        Args:
            toSend (list): the list with the value to send.
        """
        self.write_port(self.commands['Laser'])

        self.write_port(f'{toSend:02x}')

    def send_Auto_Acquisition(self, toSend):
        """Send the TimeAverage command (&) with the data in hex by the COM port.

        Args:
            toSend (list): the list with the value to send.
        """
        returnValue = None

        self.write_port(self.commands['TimeAverage'])
        # self.write_port('@')

        for value in toSend:
            returnValue = self.write_port(f'{value:04x}')

        return returnValue

    def send_Finish_Experiment(self):
        """Send the Finish command ([) by the COM port."""
        self.write_port('[')

    def send_Init_Experiment(self):
        """Send the CurveTemporal command (|) with an @ and an 1 in hex by the COM port."""
        self.write_port(self.commands['CurveTemporal'])
        self.write_port('@')

        self.write_port(f'{1:04x}')

    def send_Values_Experiment(self, toSend):
        """Send the CurveTemporal command (|) with an @ and an 1 in hex by the COM port."""
        returnValue = None

        self.write_port(self.commands['CurveTemporal'])
        # self.write_port('@')

        # self.write_port(f'{1:04x}')

        for value in toSend:
            returnValue = self.write_port(f'{value:04x}')

        return returnValue

    def send_Back_Peristaltic(self):
        """Send the BackPeris command (<) with a 0 in hex by the COM port."""
        self.write_port(self.commands['BackPeris'])

        self.write_port(f'{0:04x}')

    def send_Stop_Peristaltic(self):
        """Send the StopPeris command (=) with a 0 in hex by the COM port."""
        self.write_port(self.commands['StopPeris'])

        self.write_port(f'{0:04x}')

    def send_Forward_Peristaltic(self):
        """Send the ForwardPeris command (>) with a 0 in hex by the COM port."""
        self.write_port(self.commands['ForwardPeris'])

        self.write_port(f'{0:04x}')

    def send_BSF_Peristaltic(self, who):
        """Send the peristaltic command corresponding.

        Args:
            who (int): the number of the action of peristaltic:
                - 0: Back
                - 1: Stop
                - 2: Forward
        """
        if who == 0:
            self.send_Back_Peristaltic()

        elif who == 1:
            self.send_Stop_Peristaltic()

        else:
            self.send_Forward_Peristaltic()
