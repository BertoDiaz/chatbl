"""
Copyright (C) 2018  Heriberto J. Díaz Luis-Ravelo

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt5.QtWidgets import QWidget, QPushButton, QGroupBox, QListWidget, QMessageBox, QComboBox
from PyQt5.QtWidgets import QDesktopWidget, QLabel, QSizePolicy, QTextEdit, QLineEdit, QGridLayout
from PyQt5.QtCore import pyqtSignal, QEvent
from PyQt5.QtGui import QColor, QTextCursor
from lib import Strings


class View(QWidget):
    closeEventSignal = pyqtSignal(QEvent)

    def __init__(self, parent):
        super().__init__(parent)

        """---------------------------------------------- QPushButtons ----------------------------------------------"""
        self.btnConnect = QPushButton(Strings.connect)
        self.btnDisconnect = QPushButton(Strings.disconnect)
        self.btnDisconnect.setDisabled(True)
        self.btnScan = QPushButton(Strings.scan)
        self.btnScan.setCheckable(True)
        self.btnSend = QPushButton('Send')
        self.btnSend.setDisabled(True)
        self.btnExit = QPushButton(Strings.exitLC)
        """----------------------------------------------------------------------------------------------------------"""

        """------------------------------------------------ QLabel --------------------------------------------------"""
        self.lblScannerState = QLabel('Stopped')
        self.lblOpenPort = QLabel(Strings.portNotConnected)
        """----------------------------------------------------------------------------------------------------------"""

        """---------------------------------------------- QListWidget -----------------------------------------------"""
        self.lstDevices = QListWidget()
        self.lstDevices.setSpacing(0)
        self.lstDevices.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        """----------------------------------------------------------------------------------------------------------"""

        """----------------------------------------------- QTextEdit ------------------------------------------------"""
        self.edtMessages = QTextEdit()
        self.edtMessages.setReadOnly(True)
        """----------------------------------------------------------------------------------------------------------"""

        """----------------------------------------------- QLineEdit ------------------------------------------------"""
        self.edtSendMessage = QLineEdit()
        """----------------------------------------------------------------------------------------------------------"""

        """----------------------------------------------- QComboBox ------------------------------------------------"""
        self.cmbPorts = QComboBox()
        self.cmbPorts.setDisabled(True)
        """----------------------------------------------------------------------------------------------------------"""

        """---------------------------------------------- QGridLayout -----------------------------------------------"""
        self.layout = QGridLayout(self)
        self.messagesLayout = QGridLayout()
        self.connectLayout = QGridLayout()
        """----------------------------------------------------------------------------------------------------------"""

        """---------------------------------------------- QGroupBox ------------------------------------------------"""

        self.connectBoxLayout = QGroupBox('Found devices')
        self.messagesBoxLayout = QGroupBox('Messages')
        """----------------------------------------------------------------------------------------------------------"""

        """Set the size and the title, and the screen is centred."""
        # self.resize(300, 200)
        self.centerWindowOnScreen()
        # self.setWindowIcon(QIcon(Strings.icon))
        self.setWindowTitle(Strings.sprV2)

    def closeEvent(self, event):
        """Active the signal when the window state is changed."""
        self.closeEventSignal.emit(event)

    def centerWindowOnScreen(self):
        """Centre the dialog in the window.

        Obtain the geometry of the window, the centre is calculated and the window is moved to the centre.
        """
        windowGeometry = self.frameGeometry()
        desktopWidget = QDesktopWidget().availableGeometry().center()
        windowGeometry.moveCenter(desktopWidget)
        self.move(windowGeometry.topLeft())

    def mainWindow(self):
        """Manage the view of the window.

        If the connection is in the first step, the progress bar is removed and the widget to try the connection are
        showed. In the case that the connection is in the second step, the widget to try the connection are removed
        and the message that confirm the connection is showed. Finally, if the connection is in the third step, a
        message to inform that the connection was successful.
        """
        self.layout.addWidget(self.setConnectGroup(), 0, 0)
        self.layout.addWidget(self.setMessagesGroup(), 0, 1)
        self.layout.addWidget(self.btnExit, 1, 0, 1, 2)

    def setConnectGroup(self):
        self.connectLayout.setContentsMargins(0, 0, 0, 0)

        self.connectLayout.addWidget(self.lblScannerState, 0, 0)
        self.connectLayout.addWidget(self.btnScan, 0, 1)
        self.connectLayout.addWidget(self.lstDevices, 1, 0, 1, 2)
        self.connectLayout.addWidget(self.btnConnect, 2, 0)
        self.connectLayout.addWidget(self.btnDisconnect, 2, 1)
        self.connectLayout.addWidget(self.cmbPorts, 3, 0)
        self.connectLayout.addWidget(self.lblOpenPort, 3, 1)

        self.connectBoxLayout.setLayout(self.connectLayout)

        return self.connectBoxLayout

    def setMessagesGroup(self):
        self.messagesLayout.setContentsMargins(0, 0, 0, 0)

        self.messagesLayout.addWidget(self.edtMessages, 0, 0, 1, 2)
        self.messagesLayout.addWidget(self.edtSendMessage, 1, 0)
        self.messagesLayout.addWidget(self.btnSend, 1, 1)

        self.messagesBoxLayout.setLayout(self.messagesLayout)

        return self.messagesBoxLayout

    def addDevicesToList(self, deviceUUID, deviceName):
        self.lstDevices.addItem(deviceName + ' ' + deviceUUID)

    def clearDevicesFromList(self):
        self.lstDevices.clear()

    def setLstScannerStateText(self, text):
        self.lblScannerState.setText(text)
        self.lblScannerState.repaint()

    def setBtnDisconnectDisable(self, disable):
        self.btnDisconnect.setDisabled(disable)
        self.btnDisconnect.repaint()

    def setBtnConnectDisable(self, disable):
        self.btnConnect.setDisabled(disable)
        self.btnConnect.repaint()

    def setBtnScanState(self, state):
        self.btnScan.setChecked(state)
        self.btnScan.repaint()

    def setBtnScanDisable(self, disable):
        self.btnScan.setDisabled(disable)
        self.btnScan.repaint()

    def setBtnSendDisable(self, disable):
        self.btnSend.setDisabled(disable)
        self.btnScan.repaint()

    def setEdtMessagesSendText(self, text):
        text = 'Sent: ' + text + '\n'

        self.edtMessages.setTextColor(QColor('Green'))
        self.edtMessages.insertPlainText(text)
        self.edtMessages.moveCursor(QTextCursor.End)
        self.edtMessages.repaint()

    def setEdtMessagesReceiveText(self, text):
        text = 'Received: ' + text + '\n'

        self.edtMessages.setTextColor(QColor('Red'))
        self.edtMessages.insertPlainText(text)
        self.edtMessages.moveCursor(QTextCursor.End)
        self.edtMessages.repaint()

    def getEdtSendMessageText(self):
        return self.edtSendMessage.text()

    def clearEdtSendMessage(self):
        self.edtSendMessage.clear()
        self.edtSendMessage.repaint()

    def addComboBox(self, addPort):
        self.cmbPorts.addItem(addPort)

    def clearComboBox(self):
        self.cmbPorts.clear()

    def setCmbPortsDisable(self, disable):
        self.cmbPorts.setDisabled(disable)

    def setLblOpenPortText(self, text):
        """Change the status of the label.

        Args:
            text (str): the text of the label.
        """
        self.lblOpenPort.setText(text)

    def getLblOpenPortStatus(self):
        """Return the status of the label.

        Returns:
            str: the text of the label.
        """
        return self.lblOpenPort.text()

    def setGreenTextLabel(self):
        """Change the color of the labels."""
        self.lblOpenPort.setStyleSheet('QLabel {'
                                       'color: green;'
                                       '}')

    def closeDialog(self):
        self.accept()

    def setMessageCritical(self, message):
        """Show dialog with a message of error.

        Args:
            message (str): message to show in the dialog.
        """
        QMessageBox.critical(self, Strings.error, message)

    def setMessageExit(self):
        """Show a dialog with a question.

        Create a dialog with the question to exit of the app.

        Returns:
            bool: True, if the answer to the question is Yes, and False in the opposite case.
        """
        exitApp = QMessageBox.question(self, Strings.question, Strings.messageExit, QMessageBox.Yes | QMessageBox.No,
                                       QMessageBox.No)

        if exitApp == QMessageBox.Yes:
            return True

        else:
            return False
